-- Câu 1:
-- 1. Viết câu truy vấn để truy xuất họ tên, mã nhân viên, số điện thoại của các nhân viên điều hành điều phối các booking có số lượng hành khách lớn hơn 10 người 
-- để có biện pháp giãn cách an toàn chống dịch COVID-19 kịp thời. Lưu ý: tên các nhân viên phải được sắp xếp theo thứ tự alphabet
select bus_system.employee.employee_id as 'MSNV', bus_system.employee.last_name as 'Ho', employee.first_name AS 'Ten', employee_phonenumber.phone_number as 'SDT', booking.number_of_passengers as 'So hanh khach'
from bus_system.operating_staff, bus_system.booking, bus_system.employee, bus_system.employee_phonenumber
where bus_system.booking.number_of_passengers > 10
      and bus_system.booking.operating_staff_id = bus_system.operating_staff.staff_id
      and bus_system.operating_staff.staff_id = bus_system.employee.employee_id
      and bus_system.employee_phonenumber.employee_id = bus_system.employee.employee_id
order by bus_system.employee.first_name

-- 2. Viết câu truy vấn để truy xuất id và giá vé của những vé thanh toán bằng member card. Lưu ý: ticket ID được xếp theo thứ tự tăng dần của giá vé
select  bus_system.ticket.ticket_id as 'Ma so ve',bus_system.ticket.price as 'Gia ve'
from bus_system.ticket, bus_system.member_card
where member_card.payment_id = ticket.payment_id
order by ticket.price
-- 3. Viết câu truy vấn để truy xuất tổng các hành khách mà một chiếc xe chở và bảng số xe của nó. Lưu ý: Sắp xếp tăng dần theo tổng số lượng hành khách
select bus_system.bus.license_plate_no , SUM(trip_bus.number_of_passengers) as 'tong hanh khach'
from bus_system.bus, bus_system.trip_bus
where trip_bus.bus_license_plate_no = bus.license_plate_no
group by bus_system.bus.license_plate_no
having sum(trip_bus.number_of_passengers) > 0
order by SUM(trip_bus.number_of_passengers)
-- 4. Viết câu truy vấn tính giá tiền trung bình của các booking của từng các operating staff. Lưu ý: sắp xếp các OS theo thứ tự giảm dần của tiền trung bình
select operating_staff.staff_id, avg(booking.price) as 'gia trung binh'
from bus_system.operating_staff, bus_system.booking
where booking.operating_staff_id = operating_staff.staff_id
group by operating_staff.staff_id
having avg(booking.price) > 0
order by avg(booking.price) DESC
-- Câu 2:
-- Viết thủ tục hiển thị số lượng ghế với đầu vào là bảng số xe
DELIMITER //
CREATE PROCEDURE call_number_of_seats ( IN plateNo VARCHAR(10))
BEGIN
	SELECT bus_system.bus.license_plate_no, bus_system.bus.number_of_seats
    FROM bus_system.bus
	where plateNo = bus_system.bus.license_plate_no;
END //
DELIMITER ;
-- Viết thủ tục hiển thị tất cả xe mà nhà xe đang có
use bus_system
DELIMITER //
CREATE PROCEDURE call_bus()
BEGIN
	select bus_system.bus.license_plate_no
    from bus_system.bus;
END //
DELIMITER ;
call bus_system.call_bus()
-- Câu 3:
-- 
use bus_system
DELIMITER //
CREATE TRIGGER tg_ticket_INS
BEFORE INSERT
ON ticket FOR EACH ROW
BEGIN
	IF new.type_ticket = 'M' then INSERT member_card values (new.payment_id,'BTC000X00', 0);
    END IF;
    IF new.type_ticket = 'N' then INSERT non_member_card values (new.payment_id, new.price);
    END IF;
    IF new.type_ticket = 'T' then INSERT monthly_payment_card values (new.payment_id, 'BTC000X00', 0);
    END IF;
END; //
DELIMITER ;
-- ----------------------------- 
DELIMITER //
CREATE TRIGGER tg_ticket_DEL
BEFORE INSERT
ON ticket FOR EACH ROW
BEGIN
	
END; //
DELIMITER ;
-- Câu 4: Nhân viên tiến hành gửi tiết kiệm ngân hàng mỗi tháng đều đặn với số tiền bằng
-- <percent> % của số lương hiện tại của mình vào ngân hàng để đạt target
-- Viết function tính toán số chu kỳ ( tháng / quý / năm ) để đạt được target trên với lãi suất r%/tháng
-- Biết rằng không rút tiền ra trong suốt quá trình gửi tiết kiệm và r,percent và target không đổi trong suốt quá trình gửi
SET GLOBAL log_bin_trust_function_creators = 1;
use bus_system
DELIMITER //
create function count_month_to_reach_target(percent double(10,3),id_staff varchar(8),r double(10,3), target double(10,3))
returns int
begin
	
	declare money_in_evrm double(10,3);
    declare x double(10,3);
    declare y double(10,3);
    declare n int default 0;
    declare salary int default 0;
    
    select current_salary into @salary
    from employee
    where employee.employee_id =id_staff;
    if percent <0 then return -1;
    set n = 0;
	set money_in_evrm = @salary * percent/100;
    set x = target/((1+r) * money_in_evrm)*r + 1;
	set y = 1;
    label: WHILE y < x do
		set n = n + 1;
        set y = y * (1+r);
		end while label;
	return n;
end; //
DELIMITER ;
select bus_system.count_month_to_reach_target(50, 'AT190049', 0.01,76000);


