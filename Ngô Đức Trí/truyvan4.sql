SELECT
	mpc.card_id 'ID',
	mpc.expired_date 'Ngay het han',
	count(card_id) 'So lan thanh toan',
	sum(t.price) 'Tong tien'
FROM monthly_payment_card mpc
INNER JOIN ticket t ON t.payment_id = mpc.payment_id
GROUP BY card_id
ORDER BY sum(t.price) DESC;