DELIMITER $$
USE `bus_system`$$
CREATE PROCEDURE `spRoute_GetAll` ()
BEGIN
	SELECT r.route_id Route, r.distance Distance, e.last_name Ho, e.first_name Ten, e.email Email, e.employee_id EmployeeId
	FROM route r
	INNER JOIN employee e ON r.operating_staff_id = e.employee_id;
END$$

DELIMITER ;

CALL spRoute_GetAll();

DELIMITER $$
USE `bus_system`$$
CREATE PROCEDURE `spBusRoute_GetByNumberOfBusstop` (IN numberOfBusstopVal INT)
BEGIN
	SELECT
	R.Route,
	BR.type_route 'Route Type',
	R.Distance,
	R.Ho, R.Ten, R.Email,
	BR.number_of_busstop
	FROM bus_route BR
	INNER JOIN (
	SELECT r.route_id Route, r.distance Distance, e.last_name Ho, e.first_name Ten, e.email Email
	FROM route r
	INNER JOIN employee e ON r.operating_staff_id = e.employee_id
	) R ON R.Route = BR.route_id
    WHERE BR.number_of_busstop > numberOfBusstopVal;
END$$

DELIMITER ;

CALL spBusRoute_GetByNumberOfBusstop(20);