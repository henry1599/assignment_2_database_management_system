SELECT R.*, AVG(T.earning) Doanh_thu
FROM trip_bus T JOIN (
SELECT employee.employee_id ID, employee.last_name Ho, employee.first_name Ten, route.route_id Route
FROM employee JOIN route ON employee.employee_id = route.operating_staff_id
) R
ON T.route_id = R.Route
GROUP BY T.route_id
ORDER BY AVG(T.earning) DESC