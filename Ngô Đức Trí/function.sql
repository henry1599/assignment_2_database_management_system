USE `bus_system`;
DROP function IF EXISTS `fGetCompanyEarning`;

DELIMITER $$
USE `bus_system`$$
CREATE FUNCTION `fGetCompanyEarning` (departureTimeVal DATE, percentage INT)
RETURNS INTEGER
DETERMINISTIC
BEGIN
	DECLARE result INT DEFAULT 0;
    
	IF percentage > 100 OR percentage < 0 THEN
		SIGNAL SQLSTATE '45000'
        SET MESSAGE_TEXT = 'Percentage value from 0 to 100';
	END IF;
    
    SET result = 
    (SELECT sum(earning)
	FROM trip_bus
	WHERE departure_time BETWEEN departureTimeVal AND last_day(departureTimeVal));
RETURN result * percentage / 100;
END$$

DELIMITER ;