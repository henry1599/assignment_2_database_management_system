DROP TRIGGER IF EXISTS `bus_system`.`bus_route_contains_AFTER_INSERT`;

DELIMITER $$
USE `bus_system`$$
CREATE DEFINER = CURRENT_USER TRIGGER `bus_system`.`bus_route_contains_AFTER_INSERT` AFTER INSERT ON `bus_route_contains` FOR EACH ROW
BEGIN
	DECLARE currentBusStopCount INT DEFAULT 0;
    
    SELECT COUNT(*) INTO currentBusStopCount
    FROM bus_route_contains
    WHERE route_id = NEW.route_id;
    
    UPDATE bus_route
    SET number_of_busstop = currentBusStopCount
    WHERE route_id = NEW.route_id;
END$$
DELIMITER ;


DROP TRIGGER IF EXISTS `bus_system`.`bus_route_contains_AFTER_DELETE`;

DELIMITER $$
USE `bus_system`$$
CREATE DEFINER = CURRENT_USER TRIGGER `bus_system`.`bus_route_contains_AFTER_DELETE` AFTER DELETE ON `bus_route_contains` FOR EACH ROW
BEGIN
	DECLARE currentBusStopCount INT DEFAULT 0;
    
    SELECT COUNT(*) INTO currentBusStopCount
    FROM bus_route_contains
    WHERE route_id = OLD.route_id;
    
    UPDATE bus_route
    SET number_of_busstop = currentBusStopCount
    WHERE route_id = OLD.route_id;
END$$
DELIMITER ;


DROP TRIGGER IF EXISTS `bus_system`.`bus_route_contains_BEFORE_UPDATE`;

DELIMITER $$
USE `bus_system`$$
CREATE DEFINER = CURRENT_USER TRIGGER `bus_system`.`bus_route_contains_BEFORE_UPDATE` BEFORE UPDATE ON `bus_route_contains` FOR EACH ROW
BEGIN
	DECLARE currentBusStopCount INT DEFAULT 0;
    DECLARE errorMessage VARCHAR(255);
    
    SELECT COUNT(*) INTO currentBusStopCount
    FROM bus_route_contains
    WHERE route_id = OLD.route_id;
    
    SET errorMessage = CONCAT('The new order ', NEW.bus_stop_order, ' cannot be greater than the current number of bus stop ', currentBusStopCount);
    
    IF NEW.bus_stop_order > currentBusStopCount OR NEW.bus_stop_order < 1 THEN
		SIGNAL SQLSTATE '45000' SET MESSAGE_TEXT = errorMessage;
	END IF;
END$$
DELIMITER ;