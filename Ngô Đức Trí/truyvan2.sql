SELECT A.*
FROM (
SELECT E.*
FROM employee E JOIN assistant A ON E.employee_id = A.assistant_id
) A LEFT JOIN ON_BUS O ON A.employee_id = O.assistant_id
WHERE O.assistant_id IS NULL
ORDER BY A.first_name