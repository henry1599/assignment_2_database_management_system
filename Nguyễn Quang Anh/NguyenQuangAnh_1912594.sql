USE bus_system;

-- CÂU 1A
-- Truy xuất thông tin các chuyến tour được sắp xếp theo giá tiền price từ thấp đến cao
-- và các chuyến tour chỉ xuất phát tại bến vào buổi sáng ( quy định buổi sáng là từ 7:00 -> 12:00), đi trong ngày 21-05
SELECT T.route_id, T.tour_name, T.price, TR.trip_no, TR.departure_time
FROM TOUR T RIGHT OUTER JOIN TRIP_TOUR TR ON T.route_id = TR.route_id
WHERE HOUR(TR.departure_time)>=7 AND HOUR(TR.departure_time)<=12 AND DAY(TR.departure_time)= 21
ORDER BY T.price

-- Truy xuất thông tin của các tài xế lái xe, thông tin chuyến của các tour đi Đà Lạt
-- sắp xếp theo tuổi của tài xế
SELECT E.employee_id, E.first_name, E.last_name, E.age, E.email, E.sex, O.trip_no, O.bus_license_plate_no, O.route_id
FROM EMPLOYEE E LEFT OUTER JOIN ON_TOUR O ON E.employee_id = O.driver_id
WHERE O.route_id = 17
ORDER BY E.age

-- CÂU 1B
-- Truy xuất số chuyến đi của từng chuyến tour có price >= 100
SELECT T.route_id, COUNT(TR.route_id) as "Number of routes", T.tour_name, T.price, TR.number_of_passengers 
FROM TOUR T INNER JOIN TRIP_TOUR TR ON T.route_id = TR.route_id
WHERE T.price >= 100
GROUP BY T.route_id
ORDER BY T.price

-- Truy xuất số chuyến tour xuất phát vào buổi chiều tối( quy định buổi chiều tối là từ sau 12:00)
SELECT T.route_id, COUNT(TR.route_id) as "Number of routes", T.tour_name, T.price 
FROM TOUR T INNER JOIN TRIP_TOUR TR ON T.route_id = TR.route_id
WHERE HOUR(TR.departure_time) > 12
GROUP BY T.route_id
ORDER BY T.route_id

-- CÂU 2
/* Mỗi thành viên viết 1 thủ tục để hiển thị và 1 thủ tục có thao tác dữ liệu với các tham số đầu vào là các trường dữ liệu cần nhập 
(có validate giá trị truyền vào và hiển thị thông báo lỗi có nghĩa) */
-- PROCEDURE
-- TẠO MỘT THỦ TỤC HIỂN THỊ THÔNG TIN CÁC TOUR VÀ MÃ TÀI XẾ, BIỂN SỐ XE CỦA CÁC TOUR
DROP PROCEDURE IF EXISTS tripTourInformation
DELIMITER //
CREATE PROCEDURE tripTourInformation()
BEGIN
	SELECT T.route_id, T.tour_name, T.duration, T.price, O.driver_id, O.bus_license_plate_no
    FROM TOUR T INNER JOIN ON_TOUR O ON T.route_id = O.route_id
    GROUP BY O.driver_id
    ORDER BY T.route_id;
END //
DELIMITER ;

-- TẠO MỘT THỦ TỤC HIỂN THỊ THÔNG TIN CỦA CÁC CHUYẾN TOUR VỚI ĐẦU VÀO LÀ MÃ TOUR route_id
DROP PROCEDURE IF EXISTS tourRouteInformation
DELIMITER //
CREATE PROCEDURE tourRouteInformation(IN id INT)
BEGIN
	if(id < 16 OR id > 25) then
    signal sqlstate '45000'
    set message_text = 'This trip does not exist. Try again';
    end if;
	SELECT T.route_id, T.tour_name, T.duration, T.price, TR.trip_no, TR.number_of_passengers, TR.departure_time, TR.arrival_time
    FROM TOUR T INNER JOIN TRIP_TOUR TR ON T.route_id = TR.route_id
    WHERE T.route_id = id
    ORDER BY T.route_id;
END //
DELIMITER ;
-- CALL PROCEDURE
CALL tripTourInformation;
call tourRouteInformation(15);

-- CÂU 3 - TRIGGER
DROP TABLE IF EXISTS COUNT_TOUR;
CREATE TABLE COUNT_TOUR
(
	duration INT PRIMARY KEY,
    number_of_tour INT
);
INSERT INTO COUNT_TOUR VALUES (2,5);
INSERT INTO COUNT_TOUR VALUES (3,3);
INSERT INTO COUNT_TOUR VALUES (5,2);

DROP TRIGGER IF EXISTS triggerInsertTour;
DELIMITER //
CREATE TRIGGER triggerInsertTour
AFTER INSERT
ON TOUR FOR EACH ROW
BEGIN
	IF (NEW.duration = 2) then
		UPDATE COUNT_TOUR SET number_of_tour = number_of_tour + 1 WHERE duration = 2;
	ELSEIF (NEW.duration = 3) then
		UPDATE COUNT_TOUR SET number_of_tour = number_of_tour + 1 WHERE duration = 3;
	ELSEIF (NEW.duration = 5) then
		UPDATE COUNT_TOUR SET number_of_tour = number_of_tour + 1 WHERE duration = 5;
	END IF;
END; //
DELIMITER ;

INSERT INTO ROUTE VALUES(27, 50, 'OS200011');
INSERT INTO TOUR VALUES (27, 'saigon-dautieng', 2, 50);

DELETE FROM TOUR where route_id = 26;

SELECT * FROM COUNT_TOUR;
SELECT * FROM TOUR;


-- trigger 2
DROP TRIGGER IF EXISTS triggerDeleteTour;
DELIMITER //
CREATE TRIGGER triggerDeleteTour
AFTER DELETE
ON TOUR FOR EACH ROW
BEGIN
	DECLARE num INT;
	IF (OLD.duration = 2) then
		SELECT number_of_tour INTO num FROM COUNT_TOUR WHERE duration = 2;
        SET num = num - 1;
		UPDATE COUNT_TOUR SET number_of_tour = num WHERE duration = 2;
	ELSEIF (OLD.duration = 3) then
		SELECT number_of_tour INTO num FROM COUNT_TOUR WHERE duration = 3;
        SET num = num - 1;
		UPDATE COUNT_TOUR SET number_of_tour = num WHERE duration = 2;
	ELSEIF (OLD.duration = 5) then
		SELECT number_of_tour INTO num FROM COUNT_TOUR WHERE duration = 5;
        SET num = num - 1;
		UPDATE COUNT_TOUR SET number_of_tour = num WHERE duration = 5;
	END IF;
END; //
DELIMITER ;

-- trigger 3
DROP TRIGGER IF EXISTS triggerUpdateTour;
DELIMITER //
CREATE TRIGGER triggerUpdateTour
AFTER UPDATE
ON TOUR FOR EACH ROW
BEGIN
	UPDATE COUNT_TOUR SET number_of_tour = (SELECT COUNT(*) FROM TOUR WHERE duration = 2) WHERE duration = 2;
    UPDATE COUNT_TOUR SET number_of_tour = (SELECT COUNT(*) FROM TOUR WHERE duration = 3) WHERE duration = 3;
    UPDATE COUNT_TOUR SET number_of_tour = (SELECT COUNT(*) FROM TOUR WHERE duration = 4) WHERE duration = 4;
END; //
DELIMITER ;

UPDATE TOUR SET duration = 3 WHERE route_id = 18;

-- CÂU 4 - FUNCTION
-- Tạo một hàm tính thời gian(theo đơn vị năm) mà nhân viên phải làm để tăng mức lương từ base salary lên current salary
-- với tham số là % tăng lương sau mỗi năm và mã số nhân viên
DROP FUNCTION IF EXISTS fGetYearFromBaseSalaryToCurSalary;
DELIMITER //
CREATE FUNCTION fGetYearFromBaseSalaryToCurSalary ( percentage FLOAT, id VARCHAR(8) )
RETURNS FLOAT
DETERMINISTIC
BEGIN
	DECLARE result INT DEFAULT 0;
    DECLARE employeeBaseSalary INT;
    DECLARE employeeCurSalary INT;
    IF percentage < 0 OR percentage > 1 THEN
    SIGNAL SQLSTATE '45000'
    SET message_text = 'invalid percentage';
    END IF;
    
    IF (SELECT employee_id FROM EMPLOYEE WHERE employee_id = id IS NULL) THEN
	SIGNAL SQLSTATE '45000'
    SET message_text = 'invalid employee id';
    END IF;
    
    SET employeeBaseSalary = (SELECT base_salary FROM EMPLOYEE WHERE employee_id = id);
    SET employeeCurSalary = (SELECT current_salary FROM EMPLOYEE WHERE employee_id = id);
    
	label: LOOP
    SET result = result+1;
    IF employeeBaseSalary + result*employeeBaseSalary*percentage < employeeCurSalary THEN
		ITERATE label;
	END IF;
    LEAVE label;
    END LOOP label;
RETURN result;
END; //
DELIMITER ;

SELECT fGetYearFromBaseSalaryToCurSalary (0.1, 'AT170006');

