use `bus_system`;
-- tim ho va ten cua tai xe chay tuyen 01
select first_name, last_name
from bus_system.    employee
where employee_id in (select driver_id
						from bus_system.on_bus
					   where route_id = '01');

-- tim ho va ten, toc do danh may cua cac nhan vien dieu hanh
select employee.first_name, employee.last_name, operating_staff.typing_speed 
from bus_system.employee 
inner join bus_system.operating_staff on operating_staff.staff_id = employee.employee_id;

-- 2 cau truy van co aggreate function, group by, having, where, order by tu 2 bang tro len

-- thong ke so lan dung the thanh vien ung voi cac muc gia tien
select count(member_card.card_id) as 'freq membercard used', ticket.price
from bus_system.member_card  
inner join bus_system.ticket on ticket.payment_id = member_card.payment_id
group by ticket.price
order by ticket.price DESC;

-- thong ke gia ve trung binh cua tuyen co so luot dung ve thang nhieu hon 275 
select count(monthly_payment_card.card_id) as 'number of ticket', ticket.route_id, avg(ticket.price) as 'average price'
from bus_system.monthly_payment_card
inner join bus_system.ticket on ticket.payment_id = monthly_payment_card.payment_id
group by ticket.route_id
having count(monthly_payment_card.card_id) > 275;

-- PROCEDURE 

DROP PROCEDURE IF EXISTS Get_arrival_time;

-- lay thoi gian xuat phat cua chuyen xe bus voi tuyen bus va so thu tu chuyen di la tham so
DELIMITER $$
create procedure Get_arrival_time (
				IN routeid varchar(2),
				IN tripno int)
begin
    declare tcount int;

    
    select count(trip_no) into tcount
    from trip_bus
    where route_id = routeid and trip_no = tripno;
    -- check if route_id exists
    if(tcount!=1) then
		signal sqlstate '45000'
		set message_text = 'trip not available';
	end if;
    
    
    -- 
	select arrival_time, route_id, trip_no 
	from trip_bus
	where route_id = routeid and trip_no = tripno;
end $$
DELIMITER ;

-- lay danh sach tat ca cac tram xe bus
DROP PROCEDURE IF EXISTS Get_all_bus_stop;
-- procedure 2
DELIMITER $$
create procedure Get_all_bus_stop()
begin
		select bus_stop_id, bus_stop_name
        from bus_stop;
end $$
DELIMITER ;

-- TRIGGER

drop trigger if exists trip_bus_before_insert;


-- insert a trip -> increse number of_trip
DELIMITER $$
	create trigger trip_bus_before_insert
    before insert on trip_bus
    for each row
			begin
				declare count int;
                select count(route_id) into count
					from bus_route
					where route_id = new.route_id;
				if (count!=1) then -- chua co -> them vao
					insert into bus_route
						values (new.route_id, NULL , NULL , 1);
                    
                else -- co roi -> update number_of_trip
					update bus_route
                    set number_of_trip = number_of_trip +1
                    where route_id = new.route_id; 
                end if;
                
            end $$
DELIMITER ;


-- trigger 2: update bus_plate_no 
drop trigger if exists trip_bus_before_update;

DELIMITER && 
	create trigger trip_bus_before_update
    before update on trip_bus
    for each row
    begin
		declare time_count int;
        declare errorMessage varchar(100);
        select count(departure_time) into time_count
        from trip_bus
        where departure_time < old.arrival_time or arrival_time > old.departure_time and bus_license_plate_no = new.bus_license_plate_no;
        
-- if count > 0 -> trung gio -> loi
		set errorMessage = concat('the new bus which has license plate no: ',new.bus_license_plate_no,' is unavailable that time');
        
        if (time_count > 0) then 
				signal sqlstate '45000'
                set message_text = errorMessage;
		end if;
        
        
    end &&
DELIMITER ;

-- trigger 3
drop trigger if exists trip_bus_after_delete; 

DELIMITER $$
create trigger trip_bus_after_delete
after delete on trip_bus
for each row
	begin
    update bus_route
    set number_of_trip = number_of_trip - 1
    where route_id = old.route_id;
    end $$

DELIMITER ;


-- FUNCTION

SET GLOBAL log_bin_trust_function_creators = 1;

drop function if exists trip_has_free_ticket;


-- liet ke cac chuyen bus cua tuyen nhap vao co nguoi duoc mien phi ve
DELIMITER ^^
create function trip_has_free_ticket(routeid varchar(2))
RETURNS varchar(255)
deterministic
	begin

        declare rcount int;
        declare trip_count int;
        declare max_trip int;
        declare min_trip int;
        declare trip int;
        declare temp int;
        declare fst int;
        
        
		
        declare res varchar(255);
        
        select count(route_id) into rcount
        from bus_route
        where route_id = routeid;
        
        if(rcount!=1) then
			signal sqlstate '45000'
            set message_text = 'route not available';
		end if;
        
        select count(route_id) into trip_count
        from trip_bus
        where route_id = routeid;
		
        select min(trip_no), max(trip_no) into min_trip, max_trip
        from trip_bus
        where route_id = routeid;
        
        set fst = 0;
        set trip = min_trip;
        set temp = 0;
        set res = "";
		while trip <= max_trip do
			set temp = 0;
			select count(trip_no) into temp
            from trip_bus
			where trip_no = trip and route_id = routeid and trip_no in (
					select trip_no
					from ticket
					where price = 0 and route_id = routeid
					group by route_id, trip_no);
            if (temp > 0 and fst = 0) then set res = concat(res, trip), fst = 1;
            elseif (temp > 0) then set res = concat(res,'+',trip);
            end if;
            
            set trip = trip + 1;
		end while;
        
        
        return res;
    end; ^^
 DELIMITER ;

-- TEST
-- function
select route_id, trip_has_free_ticket(route_id)
from bus_route;

-- procedure
call Get_arrival_time('02', 206);

call Get_arrival_time('25', 201);
				
call Get_all_bus_stop();

-- trigger
insert into trip_bus
values ("01",207, 0, "2021-05-20 18:00:00", "2021-05-20 18:00:00", 0, "57C-37464");

insert into trip_bus
values ("11",201, 0, "2021-05-20 07:00:00", "2021-05-20 07:30:00", 0, "59C-42287");

update trip_bus
set bus_license_plate_no = "53N-91360"
where route_id = '01' and trip_no = 201;

delete from trip_bus
where trip_bus.departure_time = '2021-05-20 18:00:00';


