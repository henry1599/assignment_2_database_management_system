USE BUS_SYSTEM;

-- CÂU 1
-- Truy xuất thông tin các nhân viên điều hành nam có lương hiện tại trên 10 triệu / tháng 
-- và sắp xếp theo thứ tự tăng dần theo tuổi, ngoài ra các nhân viên này điều hành các tuyến xe
-- đường dài ( trên 100km )
-- --- Gộp bảng route và employee để lấy distance -----
SELECT DISTINCT E.employee_id, E.first_name, E.last_name, E.age, E.start_date, E.base_salary, E.current_salary, E.data_of_birth, E.email, E.sex, R.distance
FROM EMPLOYEE E INNER JOIN ROUTE R ON E.employee_id = R.operating_staff_id
WHERE E.sex = 'M' AND E.current_salary > 10000 AND R.distance > 100
ORDER BY E.age;


-- Truy xuất thông tin gồm họ tên, tuổi, giới tính và số lượng xe phân 
-- khối TRUNG BÌNH của các tài xế lái các chuyến truyền thống ở độ tuổi dưới assistantassistantassistant_id
-- 30, sắp xếp theo tên
-- --- Gộp bảng bên trong với bus để lấy ra thông tin trọng tải của bus -----
 SELECT DISTINCT R2.employee_id, R2.first_name, R2.last_name, R2.age, R2.sex, B.type_of_bus, COUNT(R2.employee_id) AS number_of_bus
 FROM
 (
	-- --- Gộp bảng bên trong với trip_bus để lấy ra biển số xe các xe bus -----
	SELECT DISTINCT R1.employee_id, R1.first_name, R1.last_name, R1.age, R1.sex, R1.route_id, T.bus_license_plate_no
	FROM 
	(
		-- --- Gộp bảng employee và on_bus để lấy được route_id tương ứng -----
		SELECT DISTINCT E.employee_id, E.first_name, E.last_name, E.age, E.sex, O.route_id
		FROM EMPLOYEE E RIGHT JOIN ON_BUS O ON E.employee_id = O.driver_id
	) R1 RIGHT OUTER JOIN TRIP_BUS T ON R1.route_id = T.route_id
 ) R2 INNER JOIN BUS B ON R2.bus_license_plate_no = B.license_plate_no
 WHERE B.type_of_bus = 'V' AND R2.age < 30
 GROUP BY R2.first_name
 ORDER BY R2.first_name;
 
-- Truy xuất họ, tên, tuổi, giới tính, mã tuyến, biển số xe, mã trạm dừng và tên trạm dừng
-- của các nhân viên điều hành quản lí các tuyến xe bus trong đó có các xe bus đi qua trạm
-- dừng Nguyễn Tri Phương
-- --- Gộp bảng bên trong với bus_stop để lấy ra tên của trạm dừng -----
 SELECT DISTINCT R2.employee_id, R2.first_name, R2.last_name, R2.age, R2.sex, R2.route_id, R2.bus_license_plate_no, R2.bus_stop_id, BS.bus_stop_name, BS.bus_stop_address
 FROM
 (
	-- --- Gộp bảng bên trong với bus_route_contains để lấy ra mã trạm dừng ---
	SELECT R1.employee_id, R1.first_name, R1.last_name, R1.age, R1.sex, R1.route_id, B.bus_stop_id, R1.bus_license_plate_no
    FROM
	(
		-- --- Gộp bảng bên trong với trip_bus để lấy ra biển số xe bus -----
		SELECT R.employee_id, R.first_name, R.last_name, R.age, R.sex, R.route_id, T.bus_license_plate_no
        FROM 
        (
			-- --- Gộp bảng employee và route để lấy ra route_id -----
			SELECT DISTINCT E.employee_id, E.first_name, E.last_name, E.age, E.sex, R.route_id
			FROM EMPLOYEE E RIGHT JOIN ROUTE R ON E.employee_id = R.operating_staff_id
        ) R INNER JOIN TRIP_BUS T ON R.route_id = T.route_id
	) R1 INNER JOIN BUS_ROUTE_CONTAINS B ON R1.route_id = B.route_id
    ORDER BY B.bus_stop_order
 ) R2 INNER JOIN BUS_STOP BS ON R2.bus_stop_id = BS.bus_stop_id
 WHERE BS.bus_stop_address = 'Nguyen Tri Phuong';


-- Truy xuất danh sách nhân viên gồm họ, tên, mã nhân viên, giới tính, lương hiện tại 
-- và số tour đã và đang quản lí
-- --- Gộp bảng bên trong với bảng employee để lấy ra thông tin nhân viên
SELECT E.employee_id, E.first_name, E.last_name, E.age, E.sex, E.current_salary, COUNT(E.employee_id) AS number_of_tour
FROM 
(
	-- --- Gộp bảng route với tour để lấy ra thông tin của nhân viên và tour tương ứng -----
	SELECT R.route_id, R.distance, R.operating_staff_id, T.tour_name, T.duration, T.price
	FROM ROUTE R RIGHT JOIN TOUR T ON R.route_id = T.route_id
) R1 LEFT JOIN EMPLOYEE E ON R1.operating_staff_id = E.employee_id
GROUP BY E.employee_id;


-- CÂU 2
-- Tạo một thủ tục hiển thị tất cả thông tin của nhân viên ( kể cả địa chỉ và sđt )
DELIMITER //
CREATE PROCEDURE p_EmployeeInfomation()
BEGIN
	SELECT E.employee_id, E.first_name, E.last_name, E.age, E.start_date, E.base_salary, E.current_salary, E.data_of_birth, E.email, E.sex, EP.phone_number , EA.address
    FROM EMPLOYEE E LEFT JOIN EMPLOYEE_PHONENUMBER EP ON E.employee_id = EP.employee_id
					LEFT JOIN EMPLOYEE_ADDRESS EA ON E.employee_id = EA.employee_id
	ORDER BY E.first_name;
END //
DELIMITER ;


-- Tạo một thủ tục có param truy xuất các nhân viên có mức lương hiện tại nằm giữa 2 giá trị cho trước
DELIMITER //
CREATE PROCEDURE p_GetCurrentSalary(IN small INT, IN big INT)
BEGIN
	SELECT E.employee_id, E.first_name, E.last_name, E.age, E.start_date, E.base_salary, E.current_salary, E.data_of_birth, E.email, E.sex, EP.phone_number , EA.address
    FROM EMPLOYEE E LEFT JOIN EMPLOYEE_PHONENUMBER EP ON E.employee_id = EP.employee_id
					LEFT JOIN EMPLOYEE_ADDRESS EA ON E.employee_id = EA.employee_id
	WHERE E.current_salary >= small AND E.current_salary <= big
	ORDER BY E.first_name;
END //
DELIMITER ;
CALL p_EmployeeInfomation;
CALL p_GetCurrentSalary(10000,20000);
SELECT * FROM EMPLOYEE;
DELETE FROM EMPLOYEE WHERE employee_id = 'AT170006';

-- CÂU 3
-- Tạo 1 trigger before insert để kiểm tra nhân viên được thêm vào thỏa điều kiện:
-- Trên 20 tuổi
-- Lương cơ bản <= Lương hiện tại
-- date_of_birth < start_date
CREATE TABLE EMPLOYEE_STATS
(
	type_employee VARCHAR(100) PRIMARY KEY,
    number_of_employee INT
);
INSERT INTO EMPLOYEE_STATS VALUES ('ASSISTANT',0);
INSERT INTO EMPLOYEE_STATS VALUES ('DRIVER',0);
INSERT INTO EMPLOYEE_STATS VALUES ('OPERATING STAFF',0);

DELIMITER //
CREATE TRIGGER tg_Employee_insert
AFTER INSERT
ON EMPLOYEE FOR EACH ROW
BEGIN
	IF (NEW.employee_id LIKE 'A%') then
		UPDATE EMPLOYEE_STATS SET number_of_employee = number_of_employee + 1 WHERE type_employee = 'ASSISTANT';
	ELSEIF (NEW.employee_id LIKE 'D%') then
		UPDATE EMPLOYEE_STATS SET number_of_employee = number_of_employee + 1 WHERE type_employee = 'DRIVER';
	ELSEIF (NEW.employee_id LIKE 'O%') then
		UPDATE EMPLOYEE_STATS SET number_of_employee = number_of_employee + 1 WHERE type_employee = 'OPERATING STAFF';
	END IF;
END; //
DELIMITER ;

DELIMITER //
CREATE TRIGGER tg_Employee_delete
AFTER DELETE
ON EMPLOYEE FOR EACH ROW
BEGIN
	DECLARE num INT;
	IF (OLD.employee_id LIKE 'A%') then
		SELECT number_of_employee INTO num FROM EMPLOYEE_STATS WHERE type_employee = 'ASSISTANT';
        SET num = num - 1;
        IF (num < 0) THEN
			SET num = 0;
		END IF;
		UPDATE EMPLOYEE_STATS SET number_of_employee = num WHERE type_employee = 'ASSISTANT';
	ELSEIF (OLD.employee_id LIKE 'D%') then
		SET num = num - 1;
        IF (num < 0) THEN
			SET num = 0;
		END IF;
		UPDATE EMPLOYEE_STATS SET number_of_employee = num WHERE type_employee = 'DRIVER';
	ELSEIF (OLD.employee_id LIKE 'O%') then
		SET num = num - 1;
        IF (num < 0) THEN
			SET num = 0;
		END IF;
		UPDATE EMPLOYEE_STATS SET number_of_employee = num WHERE type_employee = 'OPERATING STAFF';
	END IF;
END; //
DELIMITER ;


SELECT * FROM EMPLOYEE_PHONENUMBER;
-- Câu 4
-- Tạo một hàm tính số tháng mà một nhân viên cho trước (tìm bằng id) cần phải làm để đạt được mức lương cho trước
-- với mức tăng lương mỗi tháng cho trước
DELIMITER //
CREATE FUNCTION f_GetDurationToGetSalary ( e_id VARCHAR(8), e_desired_salary INT, e_increase_per_month INT )
RETURNS VARCHAR(500)
BEGIN
	DECLARE number_employee INT;
    DECLARE cur_sal INT;
    DECLARE months INT;
    DECLARE result VARCHAR(500);
    SET result = '';
    SET months = 0;
    SET number_employee = 0;
    SET cur_sal = 0;
    SELECT COUNT(E.employee_id) INTO number_employee FROM EMPLOYEE E WHERE E.employee_id = e_id;
    IF number_employee = 1 THEN
		SELECT E.current_salary INTO cur_sal FROM EMPLOYEE E WHERE E.employee_id = e_id;
		IF cur_sal < e_desired_salary THEN
			label1 : WHILE cur_sal < e_desired_salary DO
				SET cur_sal = cur_sal + cur_sal * e_increase_per_month / 100;
                SET months = months + 1;
                END WHILE label1;
                Set result = CONCAT(e_id,' cần làm thêm ',months,' tháng');
                RETURN result;
        END IF;
    END IF;
END; //
DELIMITER ;

SELECT f_GetDurationToGetSalary ('DR170002',50000,15);
